const Guild = require('../models/guild');
const Channel = require('../models/channel');
const mongoose = require('mongoose');

exports.AddChannel = function(req, res) {

    Guild.findById(req.params.guildId, function(err, guild) {

        if (guild.owner_id != req.user.id) {
            if (res) {
                res.status(401).json({ error: { message: "You're not authorized to do that" }, user: user});
            }
            return;
        }

        const channel = new Channel({
            _id: mongoose.Types.ObjectId(),
            name: req.body.name,
            created: new Date(),
            topic: req.body.topic,
            css: req.body.css,
            guild: req.params.guildId
        });
    
        channel.save().then(result => {
            if (res) {
                res.status(201).json({
                    message: "Success",
                    channel: {
                        _id: result._id,
                        css: result.css,
                        name: result.name,
                        created: result.created,
                        topic: result.topic,
                        guild: result.guild
                    }
                });
            }
        }).catch(err => {
            console.log(err);
            if (res) {
                res.status(500).json({error: { message: err.message }});
            }
        });
    });
};

exports.StartTyping = function(req, res) {

    if (!req.app.sockets.channels[req.params.channelId]) {
        res.status(400).json({error: {message: "Invalid channel"}});
        return;
    }

    //Send live update
    req.app.sockets.channels[req.params.channelId].forEach(client => {
        client.emit('chat-typing', {
            user: req.user.id
        });
        res.status(200).json({message: "Success"});
    });
}