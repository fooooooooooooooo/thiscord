var fs = require('fs');
const ChannelController = require('./ChannelController');
const Guild = require('../models/guild');
const mongoose = require('mongoose');

exports.Create = function(req, res) {
    if (!req.file) {
        return res.status(400).json({error: { message: "No image provided" }})
    }

    const guild = new Guild({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        css: req.body.css,
        settings: "{}",
        owner_id: req.user.id,
        created: new Date()
    });

    console.log("2");
    guild.save().then(result => {
        console.log("New guild created, welcome " + req.body.name);

        //Move the file to be SERVER_ID.extension
        fs.rename(req.file.path, req.file.destination + result._id + ".jpg");

        //Create default channel
        ChannelController.AddChannel({
            body: {
                css: "",
                name: "chat",
                topic: "Your new chat room"
            },
            params: {
                guildId: result._id
            },
            user: req.user
        });

        res.json({
            message: "Success",
            guild: {
                _id: result._id,
                name: result.name,
                css: result.css,
                settings: result.settings,
                owner_id: result.owner_id,
                created: result.created
            }
        });
        
    }).catch(err => {
        console.log("Error! " + err);;
        res.json({ error: { message: err.message }});
    });
}