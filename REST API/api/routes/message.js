const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Message = require('../models/message');

router.get('/:guildId/:channelId/', (req, res, next) => {

    //TODO: Limit by channel

    Message.find({channel_id: req.params.channelId}).sort('-sent').limit(20).exec()
        .then(result => {
            res.status(200).json(result.reverse());
        }).catch(err => {
            console.log(err);
            res.status(500);
        });
});

router.post('/:guildId/:channelId/message/send', (req, res, next) => {

    const message = new Message({
        _id: new mongoose.Types.ObjectId(),
        message: req.body.message,
        originalMessage: req.body.message,
        sender_id: req.user.id,
        guild_id: req.params.guildId,
        channel_id: req.params.channelId,
        group: 0,
        sent: new Date()
    });

    message.save().then(result => {
        
        //Send live update
        req.app.sockets.channels[req.params.channelId].forEach(client => {
            client.emit('chat-message', message);
        });

        res.json({
            message: "Success",
        });
        
    }).catch(err => {
        console.log("Error! " + err);;
        res.json({ error: { message: err.message }});
    });
});

router.get('/:guildId/:channelId/:messageId', (req, res, next) => {
    const id = req.params.guildId;

    Message.findById(id).exec().then(message => {

        if (message) {
            res.status(200).json(message);
            
        } else {
            res.status(404).json({error: { message: "No guild found" }});
        }
        
    }).catch(err => {
        console.log(err);
        res.status(500).json({ error: { message: "Invalid guild ID" }});
    });
});

router.patch('/:guildId/:channelId/:messageId', (req, res, next) => {

    const changes = {};

    //Only allow name, css and settings to be changed
    if (req.body.message) {
        changes.message = req.body.message;
    }
    
    Message.findOneAndUpdate({ _id: req.params.messageId, owner_id: req.user.id }, changes).exec().then(result => {

        if (result) {
            res.status(200).json({ message: "Success" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => {
        console.log(err);
        res.status(500).json({error: { message: err.message }});
    });

});

router.delete('/:guildId/:channelId/:messageId', (req, res, next) => {
    Message.findOneAndDelete({ _id: req.params.messageId, sender_id: req.user.id }).exec().then(result => {
        if (result) {
            res.status(200).json({ message: "Successfully deleted object" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => {
        console.log(err);
        res.status(500);
    });
});

module.exports = router;