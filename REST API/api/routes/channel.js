const express = require('express');
const router = express.Router();
const Channel = require('../models/channel');
const mongoose = require('mongoose');
const ChannelController = require('../controllers/ChannelController');

router.get('/:guildId/list', (req, res, next) => {
    Channel.find({ guild: req.params.guildId }).exec().then(channel => {

        const response = [];

        channel.forEach((element, index) => {
            response[index] = {
                _id: element._id,
                name: element.name,
                created: element.created,
                topic: element.topic,
                css: element.css
            };
        });

        res.status(200).json(response);
    }).catch(err => {
        console.log(err);
        res.status(500).json({error: { message: "Error fetching channels" }});
    });
});

router.post('/:guildId/create', (req, res, next) => ChannelController.AddChannel(req, res));

router.patch('/:guildId/:channelId', (req, res, next) => {
    const changes = {};

    //Only allow name, css and settings to be changed
    if (req.body.name) {
        changes.name = req.body.name;
    }

    if (req.body.css) {
        changes.css = req.body.css;
    }

    if (req.body.topic) {
        changes.topic = req.body.topic;
    }
    
    Channel.findByIdAndUpdate(req.params.channelId, changes).exec().then(result => {
        if (result) {
            res.status(200).json({
                _id: result._id,
                css: result.css,
                name: result.name,
                created: result.created,
                topic: result.topic
            });
        } else {
            res.status(404).json({
                message: "No channel found"
            });
        }
    }).catch(err => {
        console.log(err);
        res.status(500).json({error: { message: err }});
    });
});

router.delete('/:guildId/:channelId', (req, res, next) => {
    res.status(200).json({
        message: "Handling DELETE requests to /channel/" + req.params.guildId + "/" + req.params.channelId,
    });
});

router.post('/:guildId/:channelId/type', (req, res, next) => ChannelController.StartTyping(req, res));

module.exports = router;