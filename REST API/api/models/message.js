const mongoose = require('mongoose');

const messageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    sender_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
    guild_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Guild' },
    channel_id: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Channel' },
    message: {type: String, required: true},
    originalMessage: {type: String, required: true},
    group: {type: Number, required: true},
    sent: { type: Date, required: true },
});

module.exports = mongoose.model('Message', messageSchema);