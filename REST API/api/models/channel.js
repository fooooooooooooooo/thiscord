const mongoose = require('mongoose');

const channelSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true }, 
    css: { type: String, default: "" },
    topic: { type: String, required: true },
    created: { type: Date, required: true },
    guild: { type: mongoose.Schema.Types.ObjectId, ref: 'Guild', required: true }
});

module.exports = mongoose.model('Channel', channelSchema);