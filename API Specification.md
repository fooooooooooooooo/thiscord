# Thiscord Api Specification
The `token` item should be sent in the header as the field `Authorization` in the format `Bearer YOUR_TOKEN_HERE`

### Authentication
#### POST /auth/login
Returns a token for a user that can be used for future authentication

Requires
- `email` The users email address
- `password` The users password

Returns
- `token` Token if login was successful
- `status` Error code if unsuccessful
- `message` Error message if unsuccessful

#### POST /auth/register
Registers for an account to login with in the future

Requires
- `email` The users email address
- `name` The users username
- `password` The users password

Returns
- `message` Error message if unsuccessful

### Guilds
#### POST /guild/create
Creates a guild

Requires
- `token` Requires access token
- `name` The name of the guild

Returns
- `status` Status code

#### POST /guild/{guild-id}/users
Retrieves a list of all users in a guild

Requires
- `token` Requires access token
- `guild-id` The ID of the guild

Returns
- `status` Status code

#### POST /guild/{guild-id}/create
Creates a guild

Requires
- `token` Requires access token
- `name` The name of the channel
- `topic` The topic of the channel
- `guild-id` The ID of the guild

Returns
- `message` Status code

#### GET /guild/{guild-id}/
Gets information about a guild

Requires
- `token` Requires access token
- `guild-id` The ID of the guild

Returns
- `message` Status code
- `name` The name of the guild
- `settings` The settings of the guild
- `owner_id` The ID of the guilds owner
- `created` The date the guild was created
- `css` The CSS of the guild
- `channels`
 - `id` Id of the channel
 - `name` Name of the channel
 - `created` The time the channel was created
 - `topic` The topic of the channel
 - `css` CSS of the channel

#### PATCH /guild/{guild-id}/
Updates guild information

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `name` **[Optional]** The name of the guild
- `css` **[Optional]** The name of the guild
- `settings` **[Optional]** The settings of the guild
- `image` **[Optional]** The image of the guild

Returns
- `message` Status code

#### DELETE /guild/{guild-id}/
Deletes a guild

Requires
- `token` Requires access token
- `guild-id` The ID of the guild

Returns
- `message` Status code

### Channels
#### POST /guild/{guild-id}/create
Creates a channel

Requires
- `token` Requires access token
- `name` The name of the channel
- `topic` The topic of the channel

Returns
- `status` Status code

#### GET /guild/{guild-id}/{channel-id}
Gets information about a channel

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the guild

Returns
- `message` Status code
- `id` Id of the channel
- `name` Name of the channel
- `created` The time the channel was created
- `topic` The topic of the channel
- `css` CSS of the channel

#### PATCH /guild/{guild-id}/{channel-id}
Updates channel information

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the guild
- `name` **[Optional]** The name of the guild
- `css` **Optional]** The css of the guild
- `topic` **[Optional]** The topic of the guild

Returns
- `message` Status code

#### DELETE /guild/{guild-id}/{channel-id}
Deletes a guild

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the channel

Returns
- `message` Status code

### Message System
#### POST /guild/{guild-id}/{channel-id}/send
Sends a message into the chat of the requested channel and guild

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the channel
- `message` The content of the message

Returns
- `message` Status code

#### POST /guild/{guild-id}/{channel-id}/typing
Notifies the channel that you are typing. Needs to be repeated every 4 seconds to stay active.

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the channel

Returns
- `message` Status code

#### POST /guild/{guild-id}/{channel-id}/history
Retrieves the message history of a channel

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the channel

Returns
- `message` Status code

#### POST /guild/{guild-id}/{channel-id}/{message-id}/edit
Edits a message in chat in the requested channel and guild

Requires
- `token` Requires access token
- `guild-id` The ID of the guild
- `channel-id` The ID of the channel
- `message-id` The ID of the message
- `message` The content of the message

Returns
- `message` Status code

## Users

#### GET /users/current
Returns logged in users information

Returns
- `id` The user id
- `name` The users name
- `identifier` The users identifier
- `email` The email of the user
- `created` The time the user registered

#### GET /users/{user-id}/
Returns limited user information

Requires
- `user-id` The ID of the user

Returns
- `id` The user id
- `name` The users name
- `identifier` The users identifier
- `created` The time the user registered

#### DELETE /users/{user-id}/
Deletes a user

Requires
- `user-id` The ID of the user

Returns
- `message` Status of the query
