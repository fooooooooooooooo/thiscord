@extends('layout.app')

@section("head")
<style>
    /* Login Area */

    .login {
        width: 400px !important;
        position: absolute;
        top: 40%;
        left: 0px;
        right: 0px;
        margin-left: auto;
        margin-right: auto;
    }

    .login a {
        color: white !important;
    }

    @media only screen and (max-width: 800px) {
        .login {
            width: calc(100vw - 50px) !important;
            top: 20%;
        }
    }

    .login .profile {
        width: 70px;
        height: 70px;
        border-radius: 100px;
        margin-right: 20px;
        background-color: #484b52 !important;
    }
</style>
@endsection

@section("content")
    <form method="POST" class="register shadow" action="/register">
        @csrf

        <h3>Create an account</h3>
        <div class="form-group row">
            <div class="col">
                <label for="name" class="col-form-label text-md-right">Username</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">

            <div class="col">
                <label for="email" class="col-form-label text-md-right">E-Mail Address</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">

            <div class="col">
                <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">

            <div class="col">
                <label for="password-confirm" class="col-form-label text-md-right">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col">
                <button type="submit" class="form-control btn btn-primary">
                    Register
                </button>
            </div>
        </div>
        <a class="btn btn-link" href="/register">
            Already have an account?
        </a>
        <br>
        <span>By registering you agree to our ToS and Privacy Policy</span>
    </form>
    <script src="{{ asset('js/global.js') }}"></script>
    <link href="{{ asset("css/login.css") }}" rel="stylesheet">

@endsection