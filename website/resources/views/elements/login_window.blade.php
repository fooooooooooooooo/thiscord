<form class="login" action="/" class="container">
    <div class="row">
        <h2>ThisCord</h2>
    </div>
    <br>
    <div class="row">
        <a>Username</a>
        <input type="text" autocomplete="off" class="form-control" id="username-field" name="username" placeholder="Username">
    </div>
    <br>
    <div class="row">
        <div>
            <img class="profile" src="/storage/img/default-profile-picture.jpg">
        </div>
        <div class="col">
            <div class="row">
                <a>Profile URL</a>
                <input type="text" autocomplete="off" class="form-control" id="profile-field" placeholder="https://i.imgur.com/jeKJG.png">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <input type="submit" onclick="return Login();" value="LOGIN" class="start form-control">
    </div>
</form>