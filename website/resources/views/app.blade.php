@extends('layout.head')

@section("head")
    <script src="https://unpkg.com/vuex@3.1.1/dist/vuex.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.dev.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?lang=css&amp;skin=sunburst"></script>
@endsection

@section("content")
<body style="overflow: hidden; width: 100vw; height: 100vh;">
    <div id="app" style="overflow: hidden; width: 100vw; height: 100vh;">
        <div class="row" style="margin: 0px !important;">
            <usersettings v-if="loggedIn"></usersettings>
            <channelcreationscreen v-if="loggedIn"></channelcreationscreen>
            <guildcreationscreen v-if="loggedIn"></guildcreationscreen>
            <guildsettings v-if="loggedIn"></guildsettings>
            <smalluserdisplay v-if="loggedIn"></smalluserdisplay>
            <emojipicker></emojipicker>
            <login v-if="!loggedIn"></login>
            <navigationbar v-if="loggedIn"></navigationbar>
            <chatwindow v-if="loggedIn"></chatwindow>
            <membersbar v-if="loggedIn"></membersbar>
        </div>
    </div>
    <script src="{{ asset('js/global.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        //Let the client know who it is 
        <?php
        print("var api = '" . env("API_URL", "http://SET_API_URL_IN_ENV_FILE.com/") . "';");
        ?>

        var settings = {};
        settings.api = api;

        window.settings = settings;
        
    </script>
    <style>
        
        /* Vue is being an asshole and making these styles not apply due to the content beign dynamically added so it dosen't put its special css tag on it */

        .spoiler {
            background-color: #212121;
            border-radius: 2px;
            color: rgba(255, 255, 255, 0);
            cursor: pointer;
            padding-left: 5px;
            padding-right: 5px;
            width: auto;
        }

        .spoiler:active {
            color: rgba(255, 255, 255, 1);
        }

        #message b {
            font-weight: 1000 !important;
        }

        #message .mention {
            background-color: rgba(0, 152, 255, 0.2);
            color: rgba(0, 152, 255);
            border-radius: 5px;
            padding-left: 5px;
            padding-right: 5px;
            margin-left: 5px;
            margin-right: 5px;
            font-size: 900;
            cursor: pointer;
            user-select: none;
        }
    </style>
    <style id="guild-css">
    </style>
</body>
@endsection