const parser = require('./MsgParser');

Parser = new parser();

class MessageManager {

  GetMessages(store, guildId, channelId, cb) {

    this.store = store;

    $.GET('guild/' + guildId + "/" + channelId, function(result, data) {

        //Parse the chat
        var data = data.responseJSON;
        
        data = Parser.ParseMessages(data);

        store.commit('updateMessages', {
          guildId: guildId,
          channelId: channelId,
          messages: data
        });

        store.commit('setDisplayedMessages', {
          guildId: guildId,
          channelId: channelId
        });

        cb();
    });
  }
  SetupListeners() {
    //Listen to all incoming messages
    window.socket.on('chat-message', function(message) {
      message.displayedMessage = Parser.parseMessage(message.message);
      message.displayed = true;
      this.store.commit('addMessage', {
        message: message
      });
    }.bind(this));
  }
}

module.exports = MessageManager;