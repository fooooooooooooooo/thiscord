class WebsocketManager {
    SetupListeners() {

        if (window.socket) {
            return;
        }

        //Setup listeners to update pages
        var socket = io.connect(window.settings.api);

        window.socket = socket;

        socket.emit('auth', {
            token: window.settings.token
        });

        socket.emit('join', {
            channel: store.state.selectedGuild.channel._id
        });
    }
}

module.exports = WebsocketManager;