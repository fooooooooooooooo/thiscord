const store = new Vuex.Store({
    state: {
      user: {
        loggedIn: false
      }, 
      guilds: {}, 
      settings: {},
      selectedGuild: {
        channel: {},
        guild: {},
        messages: [],
        users: {}
      },
      users: {}
    },
    mutations: {
      login (state, user) {
        if (!user.loggedIn) {
          user.loggedIn = false;
        }
        state.user = user;
      },
      //Seperated in order to make Vue only show guilds and such once we have loaded all guilds 
      setLoggedIn (state, loggedIn) {
        state.user.loggedIn = loggedIn;
      },
      updateGuild(state, guild) {
        state.guilds[guild._id] = guild;
      },
      setSelectedGuild(state, guildId) {
        state.selectedGuild.guild = state.guilds[guildId];
      },
      updateMessages(state, update) {
        state.guilds[update.guildId].channels[update.channelId].messages = update.messages;
      },
      addMessage(state, update) {
        state.selectedGuild.messages.push(update.message);
      },
      setDisplayedMessages(state, update) {
        state.selectedGuild.messages = state.guilds[update.guildId].channels[update.channelId].messages;
      },
      setSelectedChannel(state, update) {
        state.selectedGuild.channel = state.guilds[update.guildId].channels[update.channelId];
        
      },
      addUsers(state, users) {
        users = Object.entries(users);

        users.forEach(element => {
          state.users[element[0]] = element[1];
        });
      }
    },
    actions: {
      /* Requires { guildId: id of the guild, cb: callback} */
      selectGuild(context, settings) {
        $.GET('guild/' + settings.guildId, function(data, result) {
          context.commit('updateGuild', result.responseJSON);
          context.commit('setSelectedGuild', result.responseJSON._id);
          settings.cb();
        }.bind(context));
      },
    }
  });

module.exports = store;